# README #

A looper attachment for the Eiki SL (and possibly SSL) 16mm projector. Created in Autodesk Inventor 2018.

### Materials ###

* Derlin idlers sourced from pulley-wheel.com
    * 100x40mm (3)
    * 20x40mm (6)
    * 16x42mm custom flanged (4)
* 10mm threaded rod
* 1/8in steel plate
* 45 degree aluminum angle
* Fasteners and other common parts